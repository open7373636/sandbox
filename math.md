<!--
# According to https://docs.gitlab.com/ee/user/markdown.html#math ,
# It’s possible to have math written with LaTeX syntax rendered using KaTeX.
# Math written between dollar signs $ will be rendered inline with the text.
# Math written inside a code block with the language declared as math, will be rendered on a separate line:
-->
References:
* https://en.wikipedia.org/wiki/Sensitivity_and_specificity
* https://en.wikipedia.org/wiki/Conditional_probability

Bayes' Rule:
```math
\begin{aligned}
 \Pr(A|B) &= \frac {\Pr(B|A)\Pr(A)} {\Pr(B)} \\
\end{aligned}
```

$`A`$ is the hypothesis that I have Covid-19.

$`B`$ is the evidence of a negative test result.

$`Pr(A)`$ = Prior probability that I have Covid-19 = 0.6

$`Pr(B|A)`$ = Probability of a negative test result *given that* I have Covid-19.
i.e. The probability of a *false negative*.
This can be calulated as 1 - Test Sensitivity = 0.05

$`Pr(B)`$ = Probablity of a negative test result.

This can be calculated as
```math
\Pr(B) = \Pr(B|A)\Pr(A)+\Pr(B|\neg A)\Pr(\neg A)
```

$`Pr(B|\neg A)`$ = Probability of a negative test result *given that* I do NOT have Covid-19.
This is the Test Specificity = 0.95

$`Pr(\neg A)`$ = Probability that I do NOT have Covid-19.
Calculated as $`1 - Pr(A) = 0.4`$

Therefore $`Pr(B) = 0.05 \times 0.60 + 0.95 \times 0.40 = 0.03 + 0.38 = 0.41`$

```math
\begin{aligned}
 \Pr(A|B) &= \frac {0.05 \times 0.60} {0.41} \\
          &= \frac {0.03} {0.41} \\
          &= 0.01
\end{aligned}
```