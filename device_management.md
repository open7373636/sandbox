* SNMP
* NETCONF
* oneM2M specifies 3 options:
  * BBF TR069
  * OMA DM
  * OMA LwM2M
* OpenConfig gNMI (based on gRPC)

# OMA

## LwM2M

* ARM Pelion uses LwM2M: https://www.pelion.com/iot-device-management/
* https://os.mbed.com/blog/entry/LWM2M-Meet-the-Experts-webinar/
* The [uCIFI](http://ucifi.com/) data model "leverages the Open Mobile Alliance LwM2M format." (uCIFI is a complement to [TALQ](https://www.talq-consortium.org/))

# W3C

See the note in https://gitlab.com/banjaxed/diary/-/blob/master/2020/June/17_oneM2M.md:
> I found a useful presentation: https://www.conexxus.org/sites/default/files/basic-page-file/20190813-4_Michael_McCool-Intel-WoT.pdf
> See slide "IoT Standards Map: Data – Now", which shows how oneM2M, LwM2M/IPSO, oneDM, W3C WoT, etc are all related.

# OCF

* Uses CoAP, similar to LwM2M.

* https://mynewt.apache.org/latest/os/modules/devmgmt/newtmgr.html
Apache MyNewt OS documentation:
> Use the oicmgr package if interoperability and standards-based connectivity for device interaction is your primary requirement.
> This package supports the OIC (Open Interconnect Consortium) Specification 1.1.0 framework from Open Connectivity Foundation (OCF).

* https://openconnectivity.org/developer/specifications/

* Note that OCF 1.0 was also published as ISO/IEC 30118. See https://openconnectivity.org/developer/specifications/international-standards/

* https://en.wikipedia.org/wiki/Open_Connectivity_Foundation#History

* https://iotivity.org/

## OCF and OMA cooperation

* https://omaspecworks.org/ocf-oma-liaison-pr/

* https://openconnectivity.org/open-connectivity-foundation-open-mobile-alliance-announce-liaison-agreement/

  * https://rethinkresearch.biz/articles/iotivity-and-lwm2m-love-in-under-ocf-and-oma-liaison/

From Alan Soloway (email thread https://oma.groups.io/g/dmse/topic/76102461):
> Yes, there has been a significant amount of interaction:
> - OCF referenced the LwM2M specification for device management.
> - Both OMA and OCF participated in the oneDM effort which was enabled by the LS.
> - OCF is specifying a functional bridge between the ecosystems.

See also OneDM below.

# LF Edge

## EdgeX Foundry

* https://www.lfedge.org/2020/07/23/edgex-foundry-device-actuation-from-the-cloud/
(Uses router port-forwarding in order to make the edge device reachable from the server!)

## Baetyl

* https://www.lfedge.org/2020/07/08/baetyl-2-0/
* https://www.lfedge.org/2021/06/30/baetyl-issues-2-2-release-adds-edgex-support-new-apis-debugging-and-more/

## Open Horizon

* https://www.lfedge.org/projects/openhorizon/

## Secure Device OnBoard

* https://www.lfedge.org/projects/securedeviceonboard/

* https://www.lfedge.org/2020/10/13/onboard-edge-computing-devices-with-secure-device-onboard-and-open-horizon/
> SDO technology is now being incorporated into a new industry onboarding standard being developed by the FIDO Alliance.
  * Note the link in the article from "voucher" to the FIDO spec: https://fidoalliance.org/specs/fidoiot/FIDO-IoT-spec-v1.0-wd-20200730.html. I haven't seen FIDO mentioned in the SDO spec. See section on FIDO below.

## VMWare

### Pallas

* https://www.lfedge.org/2020/07/28/exploration-and-practices-of-edge-computing-cloud-managing-virtualized-devices/
* https://flings.vmware.com/pallas#summary

### Nebula

* https://www.lfedge.org/2020/09/08/exploration-and-practices-of-edge-computing-cloud-managing-containerized-devices/

# FIDO

* June 2019: https://fidoalliance.org/fido-alliance-announces-id-and-iot-initiatives/

# IETF

## T2TRG

The Thing-to-Thing Research Group

Part of the IRTF, not the IETF?

From https://datatracker.ietf.org/rg/t2trg/about/:

> In parallel, W3C has set up an IG (Interest Group) on the Web of Things (WoT);
this is operating on a similar timeline as an RG does in the IRTF. A parallel
activity associated with the IETF/IRTF can help ensure the common work does not
stop at the traditional boundaries of the W3C, but considers networking issues
as well. IETF WGs are also active on these topics, but an IRTF RG can provide a
more long-term perspective to the collaboration.

Under "Areas of Interest":
> Management and operation of "things"
> ...
> Cooperation with W3C, e.g., on data models, formats, and semantics

# OneDM

https://onedm.org/

https://github.com/one-data-model

OMA SpecWorks statement:
https://omaspecworks.org/one-data-model-and-oma-specworks/