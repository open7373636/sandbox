# Pulse Oximeter

I purchased the SparkFun Electronics "Pulse Oximeter and Heart Rate Sensor" (p/n SEN-15219)
 36.82 from Digi-Key.

References:
* https://www.sparkfun.com/products/15219
* https://learn.sparkfun.com/tutorials/sparkfun-pulse-oximeter-and-heart-rate-monitor-hookup-guide/introduction
* https://github.com/sparkfun/SparkFun_Bio_Sensor_Hub_Library
* MAX30101
* MAX32664

 


