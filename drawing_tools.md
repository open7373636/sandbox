# Drawing Tools

Adobe Illustrator: https://www.adobe.com/ie/products/illustrator.html

Inkscape: https://inkscape.org/

https://app.diagrams.net/ (recommended by Joaquin Prado, OMA)
This used to be draw.io: https://www.diagrams.net/blog/move-diagrams-net

## Scripting

Note that diagrams.net supports
* https://mermaid-js.github.io/mermaid/#/
* [PlantUML](https://plantuml.com/)
* [GraphViz Dot](https://en.wikipedia.org/wiki/DOT_(graph_description_language))
