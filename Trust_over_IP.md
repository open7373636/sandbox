# Trust over IP

I listened to the "Trust over IP" launch event on 7th May 2020.

1) Members explain why they joined
2) Demo of problems
3) Panel with 2 other LF projects
4) Live Q&A

More than just identity.
4-layer stack for digital credentials.

Centralised roots of trust are no longer sufficient.
Layer 1 is a cryptographic web-of-trust.

Digital wallets at Layer 2.

Layer 3 is the "trust triangle".

Companies involved:
* Province of British Columbia
* evernym (https://www.evernym.com/)
* SICPA (https://www.sicpa.com/)
* IBM
* CUledger (https://www.culedger.com/)

https://trustoverip.org/members/

## More reading

Decentralised Identity / Self-Sovereign Identity (SSI)

Is this relevant only for people, or for things too?
https://www.hindawi.com/journals/jcnc/2019/8706760/

http://www.lifewithalacrity.com/2016/04/the-path-to-self-soverereign-identity.html

https://medium.com/evernym/the-three-models-of-digital-identity-relationships-ca0727cb5186

I need to understand why SSI requires a distributed ledger (blockchain):
* https://en.wikipedia.org/wiki/Public_key_infrastructure#Blockchain-based_PKI
* https://www.ledgerinsights.com/blockchain-digital-identity/

https://www.w3.org/TR/did-core/

https://identity.foundation/

https://www.hyperledger.org/

https://sovrin.org/
