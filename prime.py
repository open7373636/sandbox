x = int(input("Enter a number ..."))

# Assume that x is True
prime = True

for i in range(2, int(x**0.5)):
    if x%i == 0:
        # x is not prime!
        print(f"{x} divided by {i} = {int(x/i)}!")
        prime = False
        break

print(prime)
