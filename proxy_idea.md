We are working on reverse SSH and HTTP proxy features for WebAccess/DMP.

Then I realised that [ngrok](https://ngrok.com/) runs a business on this idea.
See also [pyngrok](https://pyngrok.readthedocs.io/en/latest/)

Write a Python script that uses AWS to create a personal server,
the Python script connects to that server to create a tunnel,
then any incoming requests to the server are forwarded on to the host machine.

Other references:
* http://serveo.net/
* https://pagekite.net/
* https://github.com/antoniomika/sish
* https://dev.to/k4ml/poor-man-ngrok-with-tcp-proxy-and-ssh-reverse-tunnel-1fm
* https://www.mysocket.io/ (launched November 2020)
* https://blog.cloudflare.com/ssh-raspberry-pi-400-cloudflare-tunnel-auditable-terminal/
