## Using VS Code and GitLab

VS Code is Git-aware, but it cannot handle a private SSH key that has a passphrase.

My typical workflow:
* From Windows explorer, right-click on a repo/directory and select "Open with Code".
* Right-click again and select "Git Bash Here".
* Make changes in CS Code and commit them locally.
* Go to the Git Bash window and "git push origin master", and you will be prompted to enter the passphrase.


## When using the GitHub Desktop client, it is not clear how to maintain a fork.

E.g. I contribute to the ipyleaflet repo.
There are 2 remotes:
- "upstream" is https://github.com/jupyter-widgets/ipyleaflet;
- "origin" is https://github.com/banjaxedben/ipyleaflet.

* Submit a PR based on a branch on origin. Afterwards (assuming it is merged) delete the branch.

* For the GitHub Desktop app, follow this article: https://github.community/t/contributing-to-repositories-with-github-desktop/10210.
i.e.
  * When you click the "Fetch origin" buton in the top taskbar, it fetches from *both* the origin and upstream remotes.
  * But there is no easy way to *pull* (fetch & merge) from upstream.
  * Then click on History, and compare with "upstream/master".
  * If there are new commits available to merge, click "Merge into master" to merge the changes from the upstream/master branch into your local master branch.
  * Push back to origin/master.