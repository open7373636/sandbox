max = 2000000

def is_prime(x):
    # Assume that x is True
    prime = True

    for i in range(2, int(x**0.5)):
        if x%i == 0:
            # x is not prime!
            #print(f"{x} divided by {i} = {int(x/i)}!")
            prime = False
            break

    return prime

count = 0

for i in range(1000000, max):
    if is_prime(i):
        print(i)
        count += 1

print(f"Found {count} primes!")
